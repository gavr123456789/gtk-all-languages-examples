Many examples: [here](https://www.programcreek.com/java-api-examples/?api=org.gnome.gtk.Label)  
More real examples: [here](http://java-gnome.sourceforge.net/doc/examples/START.html)  
Rus guide: [here](https://www.ibm.com/developerworks/ru/library/l-libjava_2/index.html)  
En guide: [here](http://zetcode.com/gui/javagnome/)  

