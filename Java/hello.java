package com.example;
 
import org.gnome.gtk.Gtk;
import org.gnome.gtk.Window;
 
public class UselessGUIApp {
    private UselessGUIApp(){
        final Window mainWindow;
        mainWindow = new Window();
        mainWindow.showAll();
    }
 
    public static void main(String[] args) {
        Gtk.init(args);
         
        new UselessGUIApp();
         
        Gtk.main();
    }
}
