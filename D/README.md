# Build
### Meson (any compiler)
`meson build`  
`ninja -C build`  
`./build/myapp` - runs
### Dmd  
`rdmd hello.d -L+gtkd`  
# Guide
[gtkdTutorial](https://sites.google.com/site/gtkdtutorial/#chapter0)

