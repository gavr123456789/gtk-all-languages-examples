[Tutorial](https://gjs-tutorial.readthedocs.io/en/latest/)  
[Better tutorial](https://gjs-guide.gitlab.io/guides/gtk/gtk-tutorial/)
You need [gjs](https://wiki.gnome.org/JavaScript) package to run this. Gjs, based on Mozilla's Spidermonkey engine.   
Run: make exec and run `./hello.js` or `gjs hello.js`  
