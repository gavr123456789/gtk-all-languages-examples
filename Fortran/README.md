[GitHub wiki](https://github.com/vmagnin/gtk-fortran/wiki)  
Build: `gfortran hello.f90 $(pkg-config --cflags --libs gtk-3-fortran)`  
Good commented program [example](https://github.com/vmagnin/gtk-fortran/blob/gtk3/examples/gtkhello.f90)  
 
